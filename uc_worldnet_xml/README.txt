=============================================================================
The WorldNet TPS Ubercart XML module V1.0 - 05_03_2010
=============================================================================

WARNING: This verion of the plug-in has been developed and tested for Ubercart version 2.3 only.

The "uc_worldnet_xml" directory shoudl be dropped into sites/all/modules/ubercart/payment/.

Once you have added these files you will need to go to the admin section of your site.

To enable the module:
Drupal Admin ->
Site Building ->
Modules ->
Enable "WorldNet XML" (in the Ubercart section) and it's dependancies.

To configure the module:
Drupal Admin ->
Store Administration ->
Configuration ->
Payment Settings ->
Payment Gateways ->
WorldNet XML Payments settings ->
Enter the details supplied by WorldNet TPS

You will need to go to:
Admin ->
Administer ->
Store Administration ->
Configuration ->
Payment Settings ->
Payment Methods ->
Credit Card Settings
and enable "Enable card owner text field on checkout form" and "Enable card type selection on checkout form".

You will also have to set up credit card encryption as per the Ubercart guidelines linked to on that page.

=============================================================================
REVISION HISTORY:

-----------------------------------------------------------------------------
V1.1 - Updated XML API to point at correct location of live XSD.

-----------------------------------------------------------------------------
V1.0 - Launched 05_03_2010
     - The WorldNet TPS Ubercart XML module V1.0

-----------------------------------------------------------------------------
=============================================================================
