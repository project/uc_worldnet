=============================================================================
The WorldNet TPS Ubercart Hosted Payment Page module V1.0 - 31_08_2010
=============================================================================

WARNING: This verion of the plug-in has been developed and tested for Ubercart version 2.3 only.

The "uc_worldnet_hpp" directory should be dropped into sites/all/modules/ubercart/payment/.

Once you have added these files you will need to go to the admin section of your site.

To enable the module:
Drupal Admin ->
Site Building ->
Modules ->
Enable "WorldNet HPP" (in the Ubercart section) and it's dependancies.

To configure the module:
Drupal Admin ->
Store Administration ->
Configuration ->
Payment Settings ->
Payment Methods ->
Tick "WorldNet HPP"
Click WorldNet HPP settings ->
Enter the details supplied by WorldNet TPS

=============================================================================
REVISION HISTORY:

-----------------------------------------------------------------------------
V1.0 - Launched 31_08_2010
     - The WorldNet TPS Ubercart HPP module V1.0

-----------------------------------------------------------------------------
=============================================================================
